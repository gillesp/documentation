# Search

The video search helps you find videos from within your instance and beyond. Videos from other instances federated by your instance (that is, instances followed by your instance, a setting your instance administrator does manually) can be found via keywords and other criteria of the advanced search.

You can also find accounts and videos not yet federated by your instance by pasting their URL directly in the search bar of your instance.
