# Remote storage (S3)

PeerTube does not support S3 buckets, but you can easily mount a S3 bucket via FUSE.
For example, you can store video files using [s3fs](https://github.com/s3fs-fuse/s3fs-fuse).

Example:

```
$ s3fs your-space-name /var/www/peertube/storage/videos -o url=https://region.digitaloceanspaces.com -o allow_other -o use_path_request_style -o uid=1000 -o gid=1000
```

There are also other (untested) tools to mount other cloud providers storage. See https://github.com/Chocobozzz/PeerTube/issues/147 for more information.
